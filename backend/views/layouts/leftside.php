<?php

use adminlte\widgets\Menu;
use yii\helpers\Html;
use yii\helpers\Url;

use common\models\Folder;

$folders = [];
$all_folders = Folder::find()->all();

for ($i=0; $i < count($all_folders); $i++) {
	$item = $all_folders[$i];
	$folders[] = [
		'label' => $item->title,
		'icon' => 'fa fa-folder',
		'url' => ["/site/folder?id=$item->id"],
		'active' => ($this->context->route == 'site/folder' and $_GET['id'] == $item->id)
	];
}

// var_dump($this->context->route); die;

?>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
	<!-- sidebar: style can be found in sidebar.less -->
	<section class="sidebar">
		<!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
				<?= Html::img('@web/img/user2-160x160.jpg', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
			</div>
			<div class="pull-left info">
				<p><?= \Yii::$app->user->identity->username ?></p>
				<a href=""><i class="fa fa-circle text-success"></i> Online</a>
			</div>
		</div>
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<?php // var_dump($folders) ?>
		<?=
			Menu::widget(
				[
					'options' => ['class' => 'sidebar-menu'],
					'items' => [
						['label' => 'Меню', 'options' => ['class' => 'header']],
						[
							'label' => 'Главная',
							'icon' => 'fa fa-home', 
							'url' => ['/'],
							'active' => $this->context->route == 'site/index'
						],
						[
							'label' => 'Ваши файлы',
							'icon' => 'fa fa-folder', 
							'url' => ['/site/myfiles'],
							'active' => $this->context->route == 'site/myfiles'
						],
						[
							'label' => 'Общие папки',
							'icon' => 'fa fa-folder',
							'url' => '#',
							'items' => $folders
						],
						[
							'label' => 'Добавить папку',
							'icon' => 'fa fa-plus', 
							'url' => ['/site/add-folder'],
							'active' => $this->context->route == 'site/add-folder'
						],
					],
				]
			)
		?>
		
	</section>
	<!-- /.sidebar -->
</aside>
