<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\data\ActiveDataProvider;
use common\models\User;

$this->title = Yii::t('app', $folder->title);
$this->params['breadcrumbs'][] = $this->title;

$dataProvider = new ActiveDataProvider([
	'query' => $model,
	'pagination' => [
	 'pageSize' => 20,
	],
]);

?>
<div class="col-md-12">
	<div class="panel panel-default">
		<div class="panel-heading"><?= $this->title ?></div>
		<div class="panel-body">

			<div class="contact-index">
					<?= Html::a(Yii::t('app','Добавить'), Url::base() . '/site/create') ?>
				<div class="fa-br"></div>
				<br>
				<?php
				echo GridView::widget([
					'dataProvider' => $dataProvider,
					'layout' => "{items}\n{pager}",
					'columns' => [
						'name',
						'date',
						'type',
						'count_downs',
						[
							'attribute' => 'user',
							'format' => 'raw',
							'value' => function($data){
								return User::find()->where(['id' => $data->user_id])->one()->username;
							},
						],
						[
							'label' => 'Ссылка',
							'format' => 'raw',
							'value' => function($data){
								return  Html::a(Yii::t('app',' Скачать'), [ '/site/down', 'id' => $data->id]);
							},
						],
					],
				]);
				?>

			</div>

		</div>
	</div>
</div>
