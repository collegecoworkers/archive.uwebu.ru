<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

use common\models\File;
use common\models\Folder;
use common\models\User;
use common\models\UserFolder;
use common\models\UploadMyFile;
use common\models\Login;
use common\models\SignUpForm;

class SiteController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [
							'signup',
							'login',
							'error'
						],
						'allow' => true,
					],
					[
						'actions' => [
							'logout',
							'index',
							'myfiles',
							'add-folder',
							'folder',
							'down',
							'create',
							'update',
							'delete',
						],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actions() {
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = File::find()->orderBy(['date' => SORT_DESC]);
		return $this->render('index', [
			'model' => $model
		]);
	}

	public function actionMyfiles() {
		if (Yii::$app->user->isGuest) return $this->goHome();

		$model = File::find()->where(['user_id' => Yii::$app->user->id])
							->orderBy(['date' => SORT_DESC]);

		return $this->render('myfiles', [
			'model' => $model
		]);
	}

	public function actionFolder($id) {
		if (Yii::$app->user->isGuest) return $this->goHome();
		$folder = Folder::find()->where(['id' => $id])->one();
		$model = File::find()->where(['folder_id' => $id]);
		return $this->render('folder', [
			'folder' => $folder,
			'model' => $model
		]);
	}

	public function actionCreate() {

		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new File();

		if (Yii::$app->request->isPost) {
			$file = UploadedFile::getInstance($model, 'hash_name');
			// var_dump(Yii::$app->request->post());die;
			if($model->saveFile($file, Yii::$app->request->post()['File']['folder_id'])) {
				return $this->redirect(['/site/myfiles']);
			}
		}

		$folders = [];
		$all_folders = Folder::find()->all();

		for ($i=0; $i < count($all_folders); $i++)
			$folders[$all_folders[$i]->id] = $all_folders[$i]->title;

		return $this->render('create', [
			'model' => $model,
			'folders' => $folders,
		]);
	}

	public function actionDelete($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = File::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

	public function actionDown($id) {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$file = File::findIdentity($id);
		
		if ($file->fileExists()) {
			$this->file_force_download($file->getFilePath(), $file->name);
			$file->count_downs += 1;
			$file->save(false);
		}

		// return $this->redirect(Yii::$app->request->referrer ?: Yii::$app->homeUrl);
	}



	public function actionAddFolder() {
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new Folder();

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->title = Yii::$app->request->post()['Folder']['title'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('add-folder', [
			'model' => $model,
		]);
	}

	public function actionSignup() {

		$this->layout = 'login';

		$model = new SignUpForm();

		if(isset($_POST['SignUpForm'])) {
			$model->attributes = Yii::$app->request->post('SignUpForm');
			if($model->validate() && $model->signup()) {
				return $this->redirect(['login']);
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}

	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		
		$this->layout = 'login';

		$model = new Login();

		if( Yii::$app->request->post('Login')) {
			$model->attributes = Yii::$app->request->post('Login');
			$user = $model->getUser();
			if($model->validate()) {
				if($user->isAdmin($user['id'])) {
					Yii::$app->user->login($user);
					return $this->goHome();
				}
			}
		}

		return $this->render('login', [
			'model' => $model,
		]);
	}

	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}

	private function file_force_download($file, $name) {
		if (file_exists($file)) {
			// сбрасываем буфер вывода PHP, чтобы избежать переполнения памяти выделенной под скрипт
			// если этого не сделать файл будет читаться в память полностью!
			if (ob_get_level()) {
				ob_end_clean();
			}
			// заставляем браузер показать окно сохранения файла
			header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . basename($name));
			header('Content-Transfer-Encoding: binary');
			header('Expires: 0');
			header('Cache-Control: must-revalidate');
			header('Pragma: public');
			header('Content-Length: ' . filesize($file));
	    // читаем файл и отправляем его пользователю
			if ($fd = fopen($file, 'rb')) {
				while (!feof($fd)) {
					print fread($fd, 1024);
				}
				fclose($fd);
			} else {
				exit;
			}
		} else {
			exit;
		}
	}
}
