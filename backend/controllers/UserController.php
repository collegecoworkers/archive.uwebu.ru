<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\User;

class UserController extends Controller
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => ['error'],
						'allow' => true,
					],
					[
						'actions' => ['index', 'update', 'delete', 'create'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionIndex() {
		$model = User::find();
		return $this->render('index', [
			'model' => $model,
		]);
	}
	
	public function actionUpdate($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = User::findIdentity($id);

		if ($model->load(Yii::$app->request->post()) && $model->validate()) {
			$model->username = Yii::$app->request->post()['User']['username'];
			$model->email = Yii::$app->request->post()['User']['email'];
			$model->level = Yii::$app->request->post()['User']['level'];
			$model->save();
			return $this->redirect(['index']);
		}

		return $this->render('update', [
			'model' => $model
		]);
	}

	public function actionDelete($id)
	{
		if (Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$model = User::findIdentity($id);
		$model->delete();
		return $this->redirect(['index']);
	}

}
