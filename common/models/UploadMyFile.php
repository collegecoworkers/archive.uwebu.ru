<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadMyFile extends Model{
    
    public $hash_name;

    public function rules()
    {
        return [
            [['hash_name'], 'required'],
            // [['hash_name'], 'file', 'extensions' => 'jpg,png']
        ];
    }

    public function uploadFile(UploadedFile $file, $currentTheFile)
    {
        $this->hash_name = $file;

       if($this->validate())
       {
           $this->deleteCurrentTheFile($currentTheFile);
           return $this->saveTheFile();
       }
    }

    private function getFolder()
    {
        // echo'<pre>';var_dump(Yii::getAlias('@backend/@web'));die;
        // return $_SERVER['DOCUMENT_ROOT'] . '/' . Yii::getAlias('@backend/@web') . "/uploads/";
    }

    private function generateFilename()
    {
        return strtolower(md5(uniqid($this->hash_name->baseName)) . '.' . $this->hash_name->extension);
    }

    public function deleteCurrentTheFile($currentTheFile)
    {
        if($this->fileExists($currentTheFile))
        {
            unlink($this->getFolder() . $currentTheFile);
        }
    }

    public function fileExists($currentTheFile)
    {
        if(!empty($currentTheFile) && $currentTheFile != null)
        {
            return file_exists($this->getFolder() . $currentTheFile);
        }
    }

    public function saveTheFile()
    {
        $filename = $this->generateFilename();

        $this->hash_name->saveAs($this->getFolder() . $filename);

        return $filename;
    }
}