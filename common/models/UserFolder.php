<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class UserFolder extends ActiveRecord {

	public static function tableName() {
		return '{{%user_folder}}';
	}

	public function rules() {
		return [];
	}
	
	public static function findId($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

}
