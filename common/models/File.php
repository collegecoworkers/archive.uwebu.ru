<?php
namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

use common\models\Folder;

class File extends ActiveRecord {

	public static function tableName() {
		return '{{%file}}';
	}

	public function rules() {
		return [
			[['name', 'type', 'hash_name'], 'string'],
			[['size', 'visible'], 'integer'],
		];
	}

	public function attributeLabels() {
		return [
			'name' => 'Название',
			'date' => 'Дата',
			'type' => 'Тип',
			'user' => 'Владелец',
			'count_downs' => 'Кол-во скачиваний',
		];
	}

	public static function findIdentity($id) {
		return static::findOne(['id' => $id]);
	}

	public function getId() {
		return $this->getPrimaryKey();
	}

	public function folderPaths(){
		return Folder::find();
	}

	public function saveProject(){

		$this->user_id = Yii::$app->user->id;
		if(!$this->date) $this->date = date('Y-m-d');

		return $this->save(false);
	}

	public function saveFile($file, $folder_id)
	{


		if($this->validate()) {
			$this->name = $file->name;
			$this->type = end(explode('.', $file->name));
			$this->size = $file->size;
			$this->visible = 1;
			$this->user_id = Yii::$app->user->id;
			$this->count_downs = 0;

			$filename = $this->generateFilename($filename, $file->extension);
			$file->saveAs($this->folderPath() . $filename);
			$this->hash_name = $filename;

			$this->folder_id = $folder_id;
		}

		return $this->save();

	}

	public function deleteFile()
	{
		if($this->fileExists($currentTheFile))
		{
			unlink($this->folderPath() . $currentTheFile);
		}
	}

	public function folderPath()
	{
		return Yii::getAlias('@backend/web') . "/uploads/";
	}

	public function getFilePath()
	{
		return $this->folderPath() . $this->hash_name;
	}

	private function generateFilename($name, $ext)
	{
		return strtolower(md5(uniqid($name)) . '.' . $ext);
	}

	public function fileExists($currentTheFile = null)
	{
		if(!empty($currentTheFile) && $currentTheFile != null) {
			return file_exists($this->folderPath() . $currentTheFile);
		} else {
			return file_exists($this->getFilePath());
		}
	}

}
