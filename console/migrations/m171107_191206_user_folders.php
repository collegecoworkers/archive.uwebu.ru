<?php

use yii\db\Migration;

class m171107_191206_user_folders extends Migration
{
    public function safeUp()
    {
        $this->createTable('user_folders', [
            'user_id' => $this->integer(),
            'folder_id' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user_folders}}');
    }
}
