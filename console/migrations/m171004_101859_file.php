<?php

use yii\db\Migration;

class m171004_101859_file extends Migration
{
    public function safeUp()
    {
        $this->createTable('file', [
            'id' => $this->primaryKey(),
            'name' => $this->string(500),
            'type' => $this->string(),
            'hash_name' => $this->string(1024),
            'size' => $this->integer(),
            'visible' => $this->integer(1),
            'user_id' => $this->integer(),
            'folder_id' => $this->integer(),
            'count_views' => $this->integer(),
            'date' => $this->date(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%file}}');
    }
}
