<?php

use yii\db\Migration;

class m171004_102550_folder extends Migration
{
    public function safeUp()
    {
        $this->createTable('folder', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'parent' => $this->integer(),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%folder}}');
    }
}
